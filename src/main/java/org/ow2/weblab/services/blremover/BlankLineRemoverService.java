/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2016 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.services.blremover;

import java.util.Iterator;
import java.util.List;
import java.util.regex.Pattern;

import javax.jws.WebService;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.ow2.weblab.core.extended.factory.ExceptionFactory;
import org.ow2.weblab.core.extended.util.ResourceUtil;
import org.ow2.weblab.core.model.Document;
import org.ow2.weblab.core.model.Resource;
import org.ow2.weblab.core.model.Text;
import org.ow2.weblab.core.services.Analyser;
import org.ow2.weblab.core.services.InvalidParameterException;
import org.ow2.weblab.core.services.analyser.ProcessArgs;
import org.ow2.weblab.core.services.analyser.ProcessReturn;
import org.purl.dc.elements.DublinCoreAnnotator;

/**
 * This simple class enables to remove successive newlines (more than two newlines), even if lines contains only spaces.
 *
 * @author BA
 */
@WebService(endpointInterface = "org.ow2.weblab.core.services.Analyser")
public class BlankLineRemoverService implements Analyser {


	/**
	 * The <code>Log</code> to be used.
	 */
	private final Log log;
	
	private static final String SERVICE_NAME = "BlankLine Remover";


	/**
	 * The default constructor that initialises the logger.
	 */
	public BlankLineRemoverService() {
		super();
		this.log = LogFactory.getLog(this.getClass());
		this.log.info("<"+SERVICE_NAME+"> successfully started and is ready to use.");
	}


	@Override
	public ProcessReturn process(final ProcessArgs args) throws InvalidParameterException {
		final List<Text> textList = this.checkArgs(args);
		final ProcessReturn pr = new ProcessReturn();
		final Resource res = args.getResource();
		final String resourceUri = res.getUri();
		final String source = new DublinCoreAnnotator(res).readSource().firstTypedValue();
		this.log.debug("<"+SERVICE_NAME+"> Start processing document <" + resourceUri + "> - <" + source + ">");
		pr.setResource(res);
		
		if (textList.isEmpty()) {
			this.log.trace("Nothing to do for " + resourceUri + " not a single non empty Text unit.");
			return pr;
		}
		final Pattern p = Pattern.compile("^\\s+$", Pattern.MULTILINE);
		for (final Text text : textList) {
			String textContent = text.getContent();
			textContent = textContent.trim();
			textContent = p.matcher(textContent).replaceAll("\n");
			textContent = textContent.replace("\r\n", "\n");
			textContent = textContent.replaceAll("[\\r\\n]{2,}|(?:\\r\\n){2,}$", "\n\n");

			text.setContent(textContent);
		}
		
		this.log.debug("<"+SERVICE_NAME+"> End processing document <" + resourceUri + "> - <" + source + ">");
		return pr;
	}


	/**
	 * Checks if <code>args</code> contains list of text sections and returns it.
	 *
	 * @param args
	 *            The <code>ProcessArgs</code>
	 * @return The <code>list of text sections</code> in <code>args</code>.
	 * @throws InvalidParameterException
	 *             If <code>args</code> is <code>null</code>, contains a <code>Resource</code> that is <code>null</code> or not a <code>MediaUnit</code>,
	 *             contains a <code>MediaUnit</code> that is not a <code>Document</code>.
	 */
	private List<Text> checkArgs(final ProcessArgs args) throws InvalidParameterException {
		if (args == null) {
			final String message = "ProcessArgs was null.";
			throw ExceptionFactory.createInvalidParameterException("<"+SERVICE_NAME+"> Unable to properly process document. " +message);
		}
		final Resource res = args.getResource();
		if (res == null) {
			final String message = "Resource in ProcessArgs was null.";
			throw ExceptionFactory.createInvalidParameterException("<"+SERVICE_NAME+"> Unable to properly process document. " +message);
		}
		if (!(res instanceof Document)) {
			final String message = "Resource in ProcessArgs was not a Document, but a " + res.getClass().getName() + "; URI of the buggy resource: " + res.getUri() + ".";
			throw ExceptionFactory.createInvalidParameterException("<"+SERVICE_NAME+"> Unable to properly process document. " +message);
		}

		final List<Text> texts = ResourceUtil.getSelectedSubResources(args.getResource(), Text.class);
		final Iterator<Text> textIt = texts.iterator();
		while (textIt.hasNext()) {
			final Text text = textIt.next();
			if (!text.isSetContent()) {
				textIt.remove();
			}
		}

		return texts;
	}

}

/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2015 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.services.blremover.test;

import java.io.File;

import org.junit.Assert;
import org.junit.Test;
import org.ow2.weblab.core.extended.exception.WebLabCheckedException;
import org.ow2.weblab.core.extended.jaxb.WebLabMarshaller;
import org.ow2.weblab.core.model.Document;
import org.ow2.weblab.core.model.Resource;
import org.ow2.weblab.core.model.Text;
import org.ow2.weblab.core.services.InvalidParameterException;
import org.ow2.weblab.core.services.analyser.ProcessArgs;
import org.ow2.weblab.core.services.analyser.ProcessReturn;
import org.ow2.weblab.services.blremover.BlankLineRemoverService;

public class RemoverTest {


	@Test
	public void testRemoverService() throws WebLabCheckedException, InvalidParameterException {
		final WebLabMarshaller marshaller = new WebLabMarshaller();
		final BlankLineRemoverService analyser = new BlankLineRemoverService();
		final Resource resource = marshaller.unmarshal(new File("src/test/resources/res_46.xml"), Resource.class);
		final String originalContent = ((Text) ((Document) resource).getMediaUnit().get(0)).getContent();
		final ProcessArgs processArgs = new ProcessArgs();
		processArgs.setResource(resource);
		final ProcessReturn processReturn = analyser.process(processArgs);
		marshaller.marshalResource(processReturn.getResource(), new File("target/res_46_cleaned.xml"));
		final String cleanedContent = ((Text) ((Document) processReturn.getResource()).getMediaUnit().get(0)).getContent();
		Assert.assertFalse("Text should not be the same.", originalContent.equals(cleanedContent));
	}


	@Test(expected = InvalidParameterException.class)
	public void testNullInput() throws InvalidParameterException {
		new BlankLineRemoverService().process(null);
	}


	@Test(expected = InvalidParameterException.class)
	public void testNullResource() throws InvalidParameterException {
		new BlankLineRemoverService().process(new ProcessArgs());
	}

}
